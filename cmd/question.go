package cmd

import (
	"database/sql"
	"strings"
	"log"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/manifoldco/promptui"
)

var questionCmd = &cobra.Command{
	Use:   "question",
	Short: "Add a new question",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		add(db, args)
	},
}

var questionListCmd = &cobra.Command{
	Use: "list",
	Short: "List questions",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		listQuestions(db)
	},
}

var questionSearchCmd = &cobra.Command{
	Use: "search",
	Short: "Search for a question",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		if len(args) == 0 {
			searchQuestion(db)
		} else {
			searchQuestionByQuery(db, args)
		}
	},
}

func init() {
	questionCmd.AddCommand(questionSearchCmd)
	questionCmd.AddCommand(questionListCmd)
	RootCmd.AddCommand(questionCmd)
}

func add(db *sql.DB, question []string) {
	insertSQL := `INSERT INTO faq(questions) VALUES (?);`

	statement, err := db.Prepare(insertSQL)
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = statement.Exec(strings.Join(question, " "))
	if err != nil {
		log.Fatal(err.Error())
	}
}

func listQuestions(db *sql.DB) {
	row, err := db.Query("SELECT questions FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	for row.Next() {
		var question string
		row.Scan(&question)
		fmt.Println(question)
	}
}

func searchQuestion(db *sql.DB) {
	row, err := db.Query("SELECT * FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []entry{}
	for row.Next() {
		var id int
		var question string
		var answer string

		row.Scan(&id, &question, &answer)

		entries = append(entries, entry{Id: id, Question: question, Answer: answer})
	}

	templates := &promptui.SelectTemplates{
		Label: "{{ . }}?",
		Active: "\U00002192 {{ .Question }} {{ .Answer }}",
		Inactive: "{{ .Question }} {{ .Answer }}",
		Selected: "\U00002192 {{ .Question }} {{ .Answer }}",
	}

	searcher := func(input string, index int) bool {
		entry := entries[index]
		question := strings.Replace(strings.ToLower(entry.Question), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(question, input)
	}

	prompt := promptui.Select{
		Label: "Select Entry",
		Items: entries,
		Templates: templates,
		Searcher: searcher,
	}

	i, _, err := prompt.Run()
	if err != nil {
		log.Fatal(err.Error())
	}

	selectedEntry := entries[i]

	fmt.Println(selectedEntry.Question + " " + selectedEntry.Answer)
}

func searchQuestionByQuery(db *sql.DB, question []string) {
	row, err := db.Query("SELECT questions, answers FROM faq WHERE questions LIKE ?", "%"+strings.Join(question, " ")+"%")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	for row.Next() {
		var question string
		var answer string
		row.Scan(&question, &answer)
		fmt.Println(question+" "+answer)
	}
}
