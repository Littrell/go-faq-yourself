package cmd

import (
	"database/sql"
	"log"
	"fmt"

	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use: "list",
	Short: "List FAQ",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		list(db)
	},
}

func init() {
	RootCmd.AddCommand(listCmd)
}

func list(db *sql.DB) {
	row, err := db.Query(`SELECT questions, answers FROM faq`)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	for row.Next() {
		var question string
		var answer string
		row.Scan(&question, &answer)
		fmt.Println(question+" "+answer)
	}
}