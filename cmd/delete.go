package cmd

import (
	"database/sql"
	"strings"
	"log"

	"github.com/spf13/cobra"
	"github.com/manifoldco/promptui"
)

var deleteCmd = &cobra.Command{
	Use: "delete",
	Short: "Delete an entry",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		delete(db)
	},
}

func init() {
	RootCmd.AddCommand(deleteCmd)
}

func delete(db *sql.DB) {
	row, err := db.Query("SELECT * FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []entry{}
	for row.Next() {
		var id int
		var question string
		var answer string

		row.Scan(&id, &question, &answer)

		entries = append(entries, entry{Id: id, Question: question, Answer: answer})
	}

	templates := &promptui.SelectTemplates{
		Label: "{{ . }}?",
		Active: "\U00002192 {{ .Question }} {{ .Answer }}",
		Inactive: "{{ .Question }} {{ .Answer }}",
		Selected: "\U00002192 {{ .Question }} {{ .Answer }}",
	}

	searcher := func(input string, index int) bool {
		entry := entries[index]
		question := strings.Replace(strings.ToLower(entry.Question), " ", "", -1)
		answer := strings.Replace(strings.ToLower(entry.Answer), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(question, input) || strings.Contains(answer, input)
	}

	prompt := promptui.Select{
		Label: "Select Entry",
		Items: entries,
		Templates: templates,
		Searcher: searcher,
	}

	i, _, err := prompt.Run()
	if err != nil {
		log.Fatal(err.Error())
	}

	selectedEntry := entries[i]

	deleteSQL := `DELETE FROM faq WHERE id = ?;`

	statement, err := db.Prepare(deleteSQL)
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = statement.Exec(selectedEntry.Id)
	if err != nil {
		log.Fatal(err.Error())
	}
}

