package cmd

import (
	"database/sql"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/manifoldco/promptui"
	_ "github.com/mattn/go-sqlite3"
)

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Creates a new default.db (in testing)",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		_, err := os.Stat("./default.faq")
		if !os.IsNotExist(err) {
			prompt := promptui.Prompt{
				Label: "./default.faq exists! Continuing will delete the current faq...continue?",
				IsConfirm: true,
			}

			result, err := prompt.Run()
			if err != nil {
				log.Fatal(err.Error())
			}

			if result == "n" {
				return
			}
		}

		os.Remove("default.faq")

		file, err := os.Create("default.faq")
		if err != nil {
			log.Fatal(err.Error())
		}
		file.Close()

		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		create(db)
	},
}

func init() {
	RootCmd.AddCommand(createCmd)
}

func create(db *sql.DB) {
	createSQL := `CREATE TABLE faq (
		"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
		"questions" TEXT,
		"answers" TEXT
	);`

	statement, err := db.Prepare(createSQL)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec()
}