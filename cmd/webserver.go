package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var webserverCmd = &cobra.Command{
	Use:   "webserver",
	Short: "When I grow up I'm going to be a webserver!",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("webserver called")
	},
}

func init() {
	RootCmd.AddCommand(webserverCmd)
}