package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var cfgFile string

type entry struct {
	Id int
	Question string
	Answer string
}

type QandA struct {
	Question string
	Answer string
}

type QandAEntries struct {
	Entries []QandA
}

var RootCmd = &cobra.Command{
	Use:   "faq",
	Short: "An faq tool for assholes",
	Long: `An faq tool for assholes`,
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}