package cmd

import (
	"database/sql"
	"strings"
	"log"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/manifoldco/promptui"
)

var answerCmd = &cobra.Command{
	Use:   "answer",
	Short: "Add a new answer",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		answer(db, args)
	},
}

var answerListCmd = &cobra.Command{
	Use: "list",
	Short: "List answers",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		listAnswers(db)
	},
}

var answerSearchCmd = &cobra.Command{
	Use: "search",
	Short: "Search for an answer",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		if len(args) == 0 {
			searchAnswer(db)
		} else {
			searchAnswerByQuery(db, args)
		}
	},
}

func init() {
	answerCmd.AddCommand(answerSearchCmd)
	answerCmd.AddCommand(answerListCmd)
	RootCmd.AddCommand(answerCmd)
}


func answer(db *sql.DB, answer []string) {
	row, err := db.Query("SELECT * FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []entry{}
	for row.Next() {
		var id int
		var question string
		var answer string

		row.Scan(&id, &question, &answer)

		entries = append(entries, entry{Id: id, Question: question, Answer: answer})
	}

	templates := &promptui.SelectTemplates{
		Label: "{{ . }}?",
		Active: "\U00002192 {{ .Question }}",
		Inactive: "{{ .Question }}",
		Selected: "\U00002192 {{ .Question }}",
	}

	searcher := func(input string, index int) bool {
		entry := entries[index]
		question := strings.Replace(strings.ToLower(entry.Question), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(question, input)
	}

	prompt := promptui.Select{
		Label: "Select Question",
		Items: entries,
		Templates: templates,
		Searcher: searcher,
	}

	i, _, err := prompt.Run()
	if err != nil {
		log.Fatal(err.Error())
	}

	selectedEntry := entries[i]

	updateSQL := `UPDATE faq SET answers = ? WHERE id = ?;`

	statement, err := db.Prepare(updateSQL)
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = statement.Exec(strings.Join(answer, " "), selectedEntry.Id)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func listAnswers(db *sql.DB) {
	row, err := db.Query("SELECT answers FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	for row.Next() {
		var answer string
		row.Scan(&answer)
		if len(answer) > 0 {
			fmt.Println(answer)
		}
	}
}

func searchAnswer(db *sql.DB) {
	row, err := db.Query("SELECT * FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []entry{}
	for row.Next() {
		var id int
		var question string
		var answer string

		row.Scan(&id, &question, &answer)

		entries = append(entries, entry{Id: id, Question: question, Answer: answer})
	}

	templates := &promptui.SelectTemplates{
		Label: "{{ . }}?",
		Active: "\U00002192 {{ .Question }} {{ .Answer }}",
		Inactive: "{{ .Question }} {{ .Answer }}",
		Selected: "\U00002192 {{ .Question }} {{ .Answer }}",
	}

	searcher := func(input string, index int) bool {
		entry := entries[index]
		answer := strings.Replace(strings.ToLower(entry.Answer), " ", "", -1)
		input = strings.Replace(strings.ToLower(input), " ", "", -1)

		return strings.Contains(answer, input)
	}

	prompt := promptui.Select{
		Label: "Select Entry",
		Items: entries,
		Templates: templates,
		Searcher: searcher,
	}

	i, _, err := prompt.Run()
	if err != nil {
		log.Fatal(err.Error())
	}

	selectedEntry := entries[i]

	fmt.Println(selectedEntry.Question + " " + selectedEntry.Answer)
}

func searchAnswerByQuery(db *sql.DB, answer []string) {
	row, err := db.Query("SELECT questions, answers from faq WHERE answers LIKE ?", "%"+strings.Join(answer, " ")+"%")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	for row.Next() {
		var question string
		var answer string
		row.Scan(&question, &answer)
		fmt.Println(question+" "+answer)
	}
}
