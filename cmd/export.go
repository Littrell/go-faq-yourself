package cmd

import (
	"encoding/json"
	"text/template"
	"database/sql"
	"encoding/csv"
	"io/ioutil"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var exportCmd = &cobra.Command{
	Use: "export",
	Short: "Export faq to different formats",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		db, _ := sql.Open("sqlite3", "./default.faq")
		defer db.Close()

		// TODO when len(args) == 0 use promptui

		// TODO turn us into proper subcommands
		if args[0] == "csv" {
			csvExport(db)
		} else if args[0] == "json" {
			jsonExport(db)
		} else if args[0] == "markdown" || args[0] == "md" {
			markdownExport(db)
		} else if args[0] == "html-page" {
			htmlPageExport(db)
		} else if args[0] == "html-component" {
			htmlComponentExport(db)
		} else {
			fmt.Println("unknown export type")
		}
	},
}

func init() {
	RootCmd.AddCommand(exportCmd)
}

func csvExport(db *sql.DB) {
	row, err := db.Query("SELECT questions, answers FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	csvEntries := [][]string{
		{"Question", "Answer"},
	}
	for row.Next() {
		var question string
		var answer string

		row.Scan(&question, &answer)

		csvEntries = append(csvEntries, []string{question, answer})
	}

	file, err := os.Create("faq.csv")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, entry := range csvEntries {
		err := writer.Write(entry)
		if err != nil {
			log.Fatal(err.Error())
		}
	}	
}

func jsonExport(db *sql.DB) {
	row, err := db.Query("SELECT questions, answers FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	jsonEntries := []QandA{}
	for row.Next() {
		var question string
		var answer string

		row.Scan(&question, &answer)

		jsonEntries = append(jsonEntries, QandA{question, answer})
	}

	res, err := json.Marshal(jsonEntries)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("faq.json", res, 0644)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func markdownExport(db *sql.DB) {
	row, err := db.Query("SELECT questions, answers FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []QandA{}
	for row.Next() {
		var question string
		var answer string

		row.Scan(&question, &answer)

		entries = append(entries, QandA{question, answer})
	}
	mdEntries := QandAEntries{Entries: entries}

	tmpl, err := template.New("").Parse(`# FAQ
{{range .Entries}}
## {{.Question}}
{{.Answer}}
{{end}}`)
	if err != nil {
		log.Fatal(err.Error())
	}

	file, err := os.Create("./faq.md")
	if err != nil {
		log.Fatal(err.Error())
	}

	err = tmpl.Execute(file, mdEntries)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func htmlPageExport(db *sql.DB) {
	row, err := db.Query("SELECT questions, answers FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []QandA{}
	for row.Next() {
		var question string
		var answer string

		row.Scan(&question, &answer)

		entries = append(entries, QandA{question, answer})
	}
	htmlEntries := QandAEntries{Entries: entries}

	tmpl, err := template.New("").Parse(`<html>
	<head></head>
	<body>
		<h1>FAQ</h1>
		{{range .Entries}}
		<h2>{{.Question}}</h2>
		<p>{{.Answer}}</p>
		{{end}}
	</body>
</html>`)
	if err != nil {
		log.Fatal(err.Error())
	}

	file, err := os.Create("./faq.html")
	if err != nil {
		log.Fatal(err.Error())
	}

	err = tmpl.Execute(file, htmlEntries)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func htmlComponentExport(db *sql.DB) {
	row, err := db.Query("SELECT questions, answers FROM faq")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer row.Close()

	entries := []QandA{}
	for row.Next() {
		var question string
		var answer string

		row.Scan(&question, &answer)

		entries = append(entries, QandA{question, answer})
	}
	htmlEntries := QandAEntries{Entries: entries}

	tmpl, err := template.New("").Parse(`{{range .Entries}}<h2>{{.Question}}</h2>
<p>{{.Answer}}</p>
{{end}}`)
	if err != nil {
		log.Fatal(err.Error())
	}

	file, err := os.Create("./faq.html")
	if err != nil {
		log.Fatal(err.Error())
	}

	err = tmpl.Execute(file, htmlEntries)
	if err != nil {
		log.Fatal(err.Error())
	}
}