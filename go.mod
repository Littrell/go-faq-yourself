module gitlab.com/Littrell/go-faq-yourself

go 1.14

require (
	github.com/manifoldco/promptui v0.8.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
)
