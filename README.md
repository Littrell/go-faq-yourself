# go-faq-yourself

An abusive `faq` command line tool for assholes

**Is this a good example of Golang best practice?** NO, screw that, I do what I want!

**Is it a work in progress?** No shit Sherlock. Have you looked at this garbage?

**Why does this exist?** Why do any of us exist, bro?! I do what I want!

**Should I use this?** That's a dumb question.

**What does it do?** Also a dumb question.

## TODO

### default.faq

What to do with this? How to specify different FAQ? Store in $HOME/.faq if a .faq doesn't exist in the current directory?

Investigate Viper as a possible solution

### Web server

`faq webserver`

### Update

`faq answer update`

`faq question update`

### Other

* There's a decent bit of duplicated code
  * Create a getter for all entries
  * Abstract promptui templates for easier reuse
* Better error handling would be sweet
* Lint the shit out of this
* Maaaaaaaaybe add some examples, you know, for losers
* Answering a question is hard
